#ifndef COUNTANDOUTPUTLETTERS_HPP
#define COUNTANDOUTPUTLETTERS_HPP

#include "ExitCode.hpp"
#include "RuntimeError.hpp"
#include "errorprint.hpp"
#include "foreachchar.hpp"

#include <algorithm>
#include <cstdint>
#include <istream>
#include <utility>

constexpr auto firstLetter{ ' ' };
constexpr auto charCount{ '~' - firstLetter + 1 };

struct LetterCountPair {
	char letter{};
	std::intmax_t count{};
};
using LetterCounts = std::array<LetterCountPair, charCount>;

inline consteval auto letterCountPairs() {
	LetterCounts letterCounts{};

	auto toAdd{ firstLetter };
	for (auto& c: letterCounts) c.letter = toAdd++;

	return letterCounts;
}

inline void updateLetterCounts(LetterCounts& letterCounts, char c) {
	using Size = LetterCounts::size_type;

	const auto index{ static_cast<Size>(c - firstLetter) };

	if (index > 0 && index < letterCounts.size())
		++letterCounts[index].count;
}

inline void countAndOutputLetters(std::istream& is) {
	auto letterCounts{ letterCountPairs() };
	if (is.fail()) throw RuntimeError{ ExitCode::badFileOpenRead, "stdin" };

	forEachChar(is, "stdin", [&](char c){
			updateLetterCounts(letterCounts, c);});

	std::ranges::sort(letterCounts,
			[](const auto& pair1, const auto& pair2){
			return pair1.count > pair2.count; });

	for (auto& pair: letterCounts) std::cout << pair.letter;

	std::cout << '\n';
}

#endif // !COUNTANDOUTPUTLETTERS_HPP
