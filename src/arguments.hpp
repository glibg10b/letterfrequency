#ifndef ARGUMENTS_HPP
#define ARGUMENTS_HPP

#include "RuntimeError.hpp"
#include "ExitCode.hpp"
#include "countandoutputletters.hpp"
#include "programname.hpp"

#include <iostream>
#include <string_view>
#include <vector>

struct Args { bool help{ false }; };

inline std::string usage() {
	return "usage:\t" + std::string{g_programName.get()} + "\n"
		"\t" +  std::string{g_programName.get()} + " --help";
}

inline void processLongOption(std::string_view option, Args& arg) {
	if (option == "help") arg.help = true;
	else throw RuntimeError{ ExitCode::badArg, "Invalid long option.\n"
		+ usage() };
}

inline Args getArgs(int argc, char* argv[]) {
	Args ret{};
	for (int i{ 1 }; i < argc; ++i)
	{
		std::string_view arg{ argv[i] };
		if (arg.starts_with("--"))
			processLongOption(arg.substr(2), ret);
		else RuntimeError{ ExitCode::badArg, "Unrecognized argument.\n"
			+ usage() };
	}

	return ret;
}

inline ExitCode::Value processArgs(const Args& args) {
	if (args.help) std::cout << usage() << '\n';
	else countAndOutputLetters(std::cin);

	return ExitCode::success;
}

#endif // !ARGUMENTS_HPP
