#include "OnceAssignable.hpp"

#include <string_view>

OnceAssignable<std::string_view> g_programName{};
