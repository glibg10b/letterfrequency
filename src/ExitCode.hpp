#ifndef EXITCODE_HPP
#define EXITCODE_HPP

#include <cstdlib>

namespace ExitCode {
	enum Value {
		success = EXIT_SUCCESS,
		badArg,
		badFileOpenRead,
		bug,
		badFileRead,
		unknown
	};
};

#endif // !EXITCODE_HPP
