#ifndef RUNTIMEERROR_HPP
#define RUNTIMEERROR_HPP

#include "ExitCode.hpp"
#include "Package.hpp"

#include <stdexcept>
#include <string_view>
#include <cassert>

class RuntimeError: public std::exception {
public:
	RuntimeError(ExitCode::Value code, std::string details)
		: m_what{ toWhat(code) + std::move(details) }
		, m_exitCode{ code }
	{}

	const char* what() const noexcept override { return m_what.c_str(); }
	ExitCode::Value exitCode() const { return m_exitCode; }
private:
	std::string toWhat(ExitCode::Value code) const {
		switch (code)
		{
		case ExitCode::badArg:
			return "invalid argument(s): ";
		case ExitCode::badFileOpenRead:
		       return "failed to open file for reading: ";
		case ExitCode::bug:
		       return "bug (please report to " PACKAGE_BUGREPORT "): ";
		case ExitCode::badFileRead:
		       return "unrecoverable error while reading from file: ";
		default:
			assert(0);
			return "unknown runtime error (please report to "
				PACKAGE_BUGREPORT "): ";
		}
	}

	std::string m_what{};
	ExitCode::Value m_exitCode{};
};

#endif // !RUNTIMEERROR_HPP
