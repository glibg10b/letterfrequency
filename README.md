# letterfrequency

A program that counts the amount of occurrences of characters from standard
input and outputs the characters sorted from most common to least common.

letterfrequency processes approximately 6 172 883 characters per second on my
machine.

# Demonstration

```sh
letterfrequency < README.md > output.md
```

output.md:

```text
etrnauolc`si.mfhd#qypgx0-)(EbkDBAR:/jIUM><4321wz$_^]O\v{|}~5@?=;9876C,+*'&%"!PZYXWVTSQ[ NLKJHGF
```

# Usage

```text
usage:	letterfrequency
	letterfrequency --help
```

## Building and installing

```sh
# 1. Extract the sources
tar xf letterfrequency-0.5.0.tar.xz

# 2. Build the executable
cd letterfrequency-0.5.0
./configure
make -j$(nproc)

# 3. Run without installing (optional)
bin/letterfrequency

# 4. Install and run the executable (installing requires root)
make install
hash -r
letterfrequency
```
